<?php defined( 'ABSPATH' ) or die( 'No direct script access!' );
/**
* Plugin Name: Display a Post
* Plugin URI: http://www.joshuawieczorek.com/wp/plugins/display-post
* Description: This is a super simple plugin that displays a specific post by post name (slug) or id. This plugin is very light weight and easy to use in pages, posts, sidebars, or anywhere shortcodes are processed.
* Version: 1.1
* Author: Joshua Wieczorek
* Author URI: http://www.joshuawieczorek.com
* License: GPL2+
*/

if( !function_exists( "jwpd_display_post" ) )
{
	function jwpd_display_post( $atts )
	{		
		// Setup shortcode 
		$a = shortcode_atts( array(
			'name'	=> false,
			'id'	=> false,
			'full'	=> false,
			'title'	=> false,
			'target'=> false,
			'color' => 'inherit',
			'tcolor'=> 'inherit'
		) , $atts );

		if( !$a['name'] && !$a['id'] ) return false;

		$target = ( $a['target'] ) ? '_blank' : '_top';

		$args['posts_per_page'] = 1;
		$args['post_type'] = 'post';
		$args['post_status'] = 'publish';

		if( $a['name'] ) {
			$args['name'] = $a['name'];
		} elseif( $a['id'] ) {
			$args['post__in'] = array( $a['id'] );
		}

		$post = get_posts( $args );
		wp_reset_postdata();

		if( !isset($post[0]) ) {
			return 'No post found!';
		}

		$html = '<div class="get-post-content-' . $post[0]->ID . '">';

		if( $a['title'] ) {
			$html .= '<h3 class="title">' . $post[0]->post_title . '</h3>';
		}
		
		if( $a['full'] ) {
			$html .= '<div class="post" style="color:' . $a['color'] . '">' . nl2br( $post[0]->post_content ) . '</div>';
		} else {
			$html .= '<div class="post" style="color:' . $a['color'] . '">' . $post[0]->post_excerpt . ' <em><a class="read-more-guid" href="' . $post[0]->guid . '" target="' . $target . '">...Continue Reading!</a></em></div>';
		}

		$html .= '</div>';
		
		return $html;
	}
}

add_shortcode( 'get-post' , 'jwpd_display_post' );