=== Display A Post ===
Contributors: jd7777
Tags: post, get post, display, display post, get, display
Donate link: http://www.joshuawieczorek.com/donate
Requires at least: 2.0
Tested up to: 4.3.1
Stable tag: 4.3.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This is a super simple plugin that displays a specific post by post name (slug) or id. This plugin is very light weight and easy to use in pages, posts, sidebars, or anywhere shortcodes are processed.

== Description ==

This is a super simple plugin that displays a specific post by post name (slug) or id. This plugin is very light weight and easy to use in pages, posts, sidebars, or anywhere shortcodes are processed.

 
= Usage =

	[get-post id="23"]

Parameters

* id = post id
* name = post's slug (do not use id and name together)
* color = color of post (e.g. #aaa) : default none
* tcolor = color of post's title (e.g. #aaa) : default none
* title = display post's title : default no (yes/no)
* full = display full post (yes/no) : default no, which displays the post's excerpt

= Lang = 

Currently this plugin is only in English, however, if anyone would like to translate it into another language please contact me at info@joshuawieczorek.com. The contribution would be greatly appreciated.

== Installation ==

1. Upload the `display-post.zip` file  to the `/wp-content/plugins/` directory
2. Activate the plugin through the Plugins Manager in the WordPress dashboard.


== Changelog ==

= Version  1.1 = 

Fixed full post output!

= Version  1.0 = 

Initial plugin release.
